package modulo234;

public class Mod2_Ejercicio2 {
	public static void main(String[]args) {

			byte bmin = -127;
			byte bmax =(byte) 128;
			// reemplazar el 0 por el valor que corresponda en todos los caso
			short smin = 32767;
			short smax =(short) 32768;
			int imax = (int)2147483648;
			int imin = (int) 2147483647;
			long lmin = (long) (Math.pow(2, 63))-1;
			long lmax = (long) (Math.pow(2, 63));
			System.out.println("Tipo de dato\tMinimo\t\t\tMaximo");
			System.out.println(".............\t......\t\t\t......");
			System.out.println("\nbyte\t\t" + bmin + "\t\t\t" + bmax);
			System.out.println("\nshort\t\t" + smin + "\t\t\t" + smax);
			System.out.println("\nint\t\t" + imin + "\t\t" + imax);
			System.out.println("\nlong\t" + lmin + "\t\t" + lmax);
	}
}
