package modelo;

import java.util.Objects;

import modelo.Practica7; //se importa la clase Practica7 del paquete modelo

@SuppressWarnings("unused")
public class Alumno extends Practica7{
	public int legajo;
	public Alumno() {
		legajo=123;
	}
	public Alumno (int legajo) {
		this.legajo=legajo;
	}
	public Alumno (String apellido, String nombre, int legajo) {
		super(apellido,nombre);
		this.legajo=legajo;
	}
	public int getlegajo() {
		return legajo;
	}
public void setLegajo(int legajo) {
	this.legajo=legajo;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = super.hashCode();
	result = prime * result + Objects.hash(legajo);
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (!super.equals(obj))
		return false;
	if (getClass() != obj.getClass())
		return false;
	Alumno other = (Alumno) obj;
	return legajo == other.legajo;
}
@Override
public String toString() {
	return "Alumno [legajo=" + legajo + "]";
}
public static void main(String[]args) {
	
}

		
}
