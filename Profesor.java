package modelo;

import java.util.Objects;

import modelo.Practica7; //se importa la clase Practica7 del paquete modelo

@SuppressWarnings("unused")
public class Profesor extends Practica7{
		public String iosfa;
		public Profesor() {
			iosfa="ejemplo";
		}
		public Profesor(String iosfa) {
			this.iosfa=iosfa;
		}
		public Profesor(String apellido, String nombre, String iosfa) {
			super(apellido, nombre);
			this.iosfa=iosfa;
		}
		public String getIosfa() {
			return iosfa;
		}
		public void setIosfa(String iosfa) {
			this.iosfa=iosfa;
		}
		//metodo hashCode

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + Objects.hash(iosfa);
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Profesor other = (Profesor) obj;
			return Objects.equals(iosfa, other.iosfa);
		}
		//metodo toString
		@Override
		public String toString() {
			return "Profesor [iosfa=" + iosfa + "]";
		}
		public static void main(String[]args) {
			
		}
}
