//declarar paquete llamado modelo
package modelo;

import java.util.Objects;

//declarar clase llamada Practica 7
public class Practica7 {
	
	//atributos
	public String apellido;
	public String nombre;
	
	public Practica7(){
		apellido="Calvo";
		nombre="Marina";
	}
	

	public Practica7(String apellido,String nombre) {
		this.apellido=apellido;
		this.nombre=nombre;
		
	}
	
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido=apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre=nombre;
	}
	
	
	//metodo equals hashCode

	@Override
	public int hashCode() {
		return Objects.hash(apellido, nombre);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Practica7 other = (Practica7) obj;
		return Objects.equals(apellido, other.apellido) && Objects.equals(nombre, other.nombre);
	}
	
	@Override
	public String toString() {
		return "Practica7 [apellido=" + apellido + ", nombre=" + nombre + "]";
	}
	public static void main(String[]args) {
		
	}
	
	
	
}
